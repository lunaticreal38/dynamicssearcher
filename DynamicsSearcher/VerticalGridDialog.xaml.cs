﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DynamicsSearcher
{
    /// <summary>
    /// 縦表示用のDataGridダイアログ
    /// </summary>
    public partial class VerticalGridDialog : Window
    {
        private string[] columnNames;
        private string[] rowNames;
        private object[,] data;


        /// <summary>
        /// コンストラクタ ※デザイナ用
        /// </summary>
        public VerticalGridDialog()
        {
            InitializeComponent();
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="columnNames">列ヘッダに表示する文字列の配列</param>
        /// <param name="rowNames">行ヘッダに表示する文字列の配列</param>
        /// <param name="data">グリッド表示する多次元配列。1次元目が列、2次元目が行の位置に対応</param>
        public VerticalGridDialog(string[] columnNames, string[] rowNames, object[,] data)
        {
            InitializeComponent();
            this.columnNames = columnNames;
            this.rowNames = rowNames;
            this.data = data;

            foreach (var columnName in columnNames)
            {
                var clm = new DataGridTextColumn() { Header = columnName };
                clm.Binding = new Binding(string.Format("{0}[{1}]",  nameof(DspItem.Data), columnName));
                this.grid.Columns.Add(clm);
            }
        }

        /// <summary>
        /// ウィンドウ読み込み時処理
        /// TODO Loadedイベントはリモートデスクトップ上だと複数動作してしまう。余裕があれば1度だけ動くよう制御する
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var dspItemList = await Task.Factory.StartNew(() =>
            {
                var list = new List<DspItem>();
                try
                {
                    for (int rowIdx = 0; rowIdx < rowNames.Length; rowIdx++)
                    {
                        var dic = new Dictionary<string, object>();
                        for (int clmIdx = 0; clmIdx < columnNames.Length; clmIdx++)
                        {
                            dic[columnNames[clmIdx]] = data[clmIdx, rowIdx];
                        }
                        list.Add(new DspItem(rowNames[rowIdx], dic));
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                return list;
            });

            this.grid.ItemsSource = dspItemList;
        }
    }

    /// <summary>
    /// 画面表示用
    /// </summary>
    public class DspItem
    {
        /// <summary>
        /// 行ヘッダ文字列
        /// </summary>
        public string Header { get; private set; }
        /// <summary>
        /// 列ヘッダ文字列をキーとしたデータ
        /// </summary>
        public Dictionary<string, object> Data { get; private set; }


        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="header"></param>
        /// <param name="data"></param>
        public DspItem(string header, Dictionary<string, object> data)
        {
            this.Header = header;
            this.Data = data;
        }
    }
}
