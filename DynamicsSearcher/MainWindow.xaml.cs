﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DynamicsSearcher
{
    /// <summary>
    /// .Net Framework4.5.0で動作するDynamics365検索アプリ
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// DataGridバインド用のフィールド入力オブジェクト
        /// </summary>
        public class FieldItem
        {
            private string _fieldName;
            private string _filterName;

            /// <summary>
            /// 検索フィールド名称を取得・設定します。
            /// </summary>
            public string FieldName
            {
                get { return _fieldName; }
                set
                {
                    if (_fieldName == value) return;
                    _fieldName = value.Trim();
                }
            }
            /// <summary>
            /// 検索フィールドに対するフィルタを設定します。
            /// </summary>
            public string FilterName
            {
                get { return _filterName; }
                set
                {
                    if (_filterName == value) return;
                    _filterName = value.Trim();
                }
            }


            /// <summary>
            /// コンストラクタ
            /// </summary>
            public FieldItem()
            {
                FieldName = "";
                FilterName = "";
            }
        }

        /// <summary>
        /// 接続状態を取得・設定します。
        /// </summary>
        public bool IsConnected
        {
            get { return (bool)GetValue(IsConnectedProperty); }
            set { SetValue(IsConnectedProperty, value); }
        }
        /// <summary>
        /// 接続状態依存プロパティです。
        /// </summary>
        public static readonly DependencyProperty IsConnectedProperty =
            DependencyProperty.Register("IsConnected", typeof(bool), typeof(MainWindow));
        /// <summary>
        /// 検索入力欄を取得・設定します。
        /// </summary>
        public ObservableCollection<FieldItem> FieldList
        {
            get { return (ObservableCollection<FieldItem>)GetValue(FieldListProperty); }
            set { SetValue(FieldListProperty, value); }
        }
        /// <summary>
        /// 検索入力欄依存プロパティです。
        /// </summary>
        public static readonly DependencyProperty FieldListProperty =
            DependencyProperty.Register("FieldList", typeof(ObservableCollection<FieldItem>), typeof(MainWindow));
        /// <summary>
        /// 検索結果欄を取得・設定します。
        /// </summary>
        public DataTable ResultData
        {
            get { return (DataTable)GetValue(ResultDataProperty); }
            set { SetValue(ResultDataProperty, value); }
        }
        /// <summary>
        /// 検索結果欄依存プロパティです。
        /// </summary>
        public static readonly DependencyProperty ResultDataProperty =
            DependencyProperty.Register("ResultData", typeof(DataTable), typeof(MainWindow));

        /// <summary>
        /// 検索中かどうかを取得します。 // TODO 余裕があれば画面にも検索中のStoryboardを追加
        /// ※検索ボタン連打対策
        /// </summary>
        public bool IsSearching
        {
            get { return (bool)GetValue(IsSearchingProperty); }
            set { SetValue(IsSearchingProperty, value); }
        }
        /// <summary>
        /// 検索中依存プロパティです。
        /// </summary>
        public static readonly DependencyProperty IsSearchingProperty =
            DependencyProperty.Register("IsSearching", typeof(bool), typeof(MainWindow));


        /// <summary>
        /// 全フィールド検索するかどうかを取得します。
        /// </summary>
        public bool IsAllAttributes
        {
            get { return (bool)GetValue(IsAllAttributesProperty); }
            set { SetValue(IsAllAttributesProperty, value); }
        }
        /// <summary>
        /// 全フィールド検索依存プロパティ
        /// </summary>
        public static readonly DependencyProperty IsAllAttributesProperty =
            DependencyProperty.Register("IsAllAttributes", typeof(bool), typeof(MainWindow), new PropertyMetadata(true));

        private DynamicsConnection dyCon = null;


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();
            FieldList = new ObservableCollection<FieldItem>();
            this.DataContext = this;

            // コマンドパラメータの先頭からURL,ユーザ,パスワードを読み込んで入力欄にセット
            var args = Environment.GetCommandLineArgs();

            if (args.Length >= 2) this.urlTxt.Text = args[1];
            if (args.Length >= 3) this.userTxt.Text = args[2];
            if (args.Length >= 4) this.passTxt.Text = args[3];
        }


        /// <summary>
        /// UIスレッド上で表示欄のテキストを設定します。
        /// </summary>
        /// <param name="text">表示文字列</param>
        private void SetDspTxt(string text)
        {
            if (!App.Current.Dispatcher.CheckAccess())
            {
                // ディスパッチャから再帰呼び出し
                App.Current.Dispatcher.Invoke(new Action<string>(SetDspTxt), text);
                return;
            }
            this.dspTxt.Text = text;
        }
        /// <summary>
        /// フィルター入力されている項目を使って絞り込み構文を作成します。
        /// </summary>
        /// <param name="fieldItems">フィルタ入力情報</param>
        /// <returns>絞り込み構文</returns>
        private FilterExpression CreateFilterExpression(IEnumerable<FieldItem> fieldItems)
        {
            var filter = new FilterExpression(LogicalOperator.And);
            foreach (var item in fieldItems)
            {
                if (string.IsNullOrEmpty(item.FilterName)) continue;
                filter.AddCondition(item.FieldName, ConditionOperator.Equal, item.FilterName); // できれば文字列はOrで判定したい
            }
            return filter;
        }
        /// <summary>
        /// 検索結果から画面表示用のDataTableを作成します。
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        private DataTable CreateResultData(EntityCollection result)
        {
            if (result == null || !result.Entities.Any()) return new DataTable();

            var dt = new DataTable(result.EntityName);
            foreach (var clm in result.Entities.SelectMany(p => p.Attributes.Keys).Distinct())
            {
                dt.Columns.Add(clm);
            }
            foreach (var entity in result.Entities)
            {
                var attrs = entity.Attributes;
                var row = new object[dt.Columns.Count];
                foreach (DataColumn clm in dt.Columns)
                {
                    row[dt.Columns.IndexOf(clm)] = attrs.GetDspValue(clm.ColumnName);
                }
                dt.Rows.Add(row);
            }
            foreach (DataColumn clm in dt.Columns)
            {
                clm.ColumnName = clm.ColumnName.Replace("_", "__"); // AutoGenerateColumnでバインドされるときに"_"はアクセスキー扱いされてしまうため"__"にする
            }
            return dt;
        }

        
        /// <summary>
        /// 接続ボタンクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void connectionBtn_Click(object sender, RoutedEventArgs e)
        {
            // 先に接続状態を確認し、接続中なら捨てる
            if (dyCon != null)
            {
                dyCon.Dispose();
                dyCon = null;
                IsConnected = false;
                SetDspTxt("切断しました");
                return;
            }
            SetDspTxt("接続中・・・");
            var url = this.urlTxt.Text;
            var user = this.userTxt.Text;
            var pass = this.passTxt.Text;
            dyCon = await Task.Factory.StartNew(() =>
            {
                try
                {
                    var conn = new DynamicsConnection(url, user, pass);
                    var whoAmIResponse = conn.Execute<WhoAmIResponse>(new WhoAmIRequest());

                    SetDspTxt("接続成功" + Environment.NewLine
                         + string.Join(Environment.NewLine, whoAmIResponse.Results.Select(p => string.Format("【{0}】{1}{2}", p.Key, Environment.NewLine, p.Value))));
                    return conn;
                }
                catch (Exception ex)
                {
                    SetDspTxt("接続失敗" + Environment.NewLine + Environment.NewLine + ex.Message);
                    return null;
                }
            });
            IsConnected = dyCon != null;
        }
        /// <summary>
        /// 検索ボタンクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            if (!IsConnected) return;
            if (IsSearching) return;
            try
            {
                IsSearching = true;
                var now = DateTime.Now;
                var logicalName = this.entityTxt.Text;
                var fieldItems = this.FieldList.Where(p => !string.IsNullOrEmpty(p.FieldName)).ToList();
                SetDspTxt(string.Format("検索開始 [{0}]", now.ToLongTimeString()));
                ResultData = new DataTable();

                var queryFields = IsAllAttributes ? Enumerable.Empty<string>() : fieldItems.Select(p => p.FieldName).Distinct().ToArray();
                var result = await Task.Factory.StartNew(() =>
                {
                    try
                    {
                        var query = dyCon.ConvertQueryExpression(logicalName, queryFields);
                        var filter = new FilterExpression(LogicalOperator.And);
                        query.Criteria.AddFilter(CreateFilterExpression(fieldItems));
                        var res = dyCon.GetEntityCollection(query);
                        return CreateResultData(res);
                    }
                    catch (Exception ex)
                    {
                        SetDspTxt("検索失敗" + Environment.NewLine + Environment.NewLine + ex.Message);
                        return null;
                    }
                });
                if (result == null) return;

                var edDate = DateTime.Now;
                var retStr = string.Format("検索終了 [{1}]{0}時間 :{2}秒{0}件数 :{3}件",
                                           Environment.NewLine,
                                           edDate.ToLongTimeString(),
                                           (edDate - now).TotalSeconds.ToString("F2"),
                                           result.Rows.Count);
                SetDspTxt(retStr);
                ResultData = result;
            }
            catch (Exception ex)
            {
                SetDspTxt("検索失敗" + Environment.NewLine + Environment.NewLine + ex.Message);
            }
            finally
            {
                IsSearching = false;
            }
        }
        /// <summary>
        /// Entity名入力テキストボックスキーダウンイベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void entityTxt_KeyDown(object sender, KeyEventArgs e)
        {
            // EnterまたはReturnキーを押した場合は検索と同じ動きにする ※IMEは入力させない
            if(e.Key == Key.Enter || e.Key == Key.Return)
            {
                SearchButton_Click(null, null);
            }
        }
        /// <summary>
        /// 選択行の縦表示を行います。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VerticalWindow_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var columnNames = this.resultGrid.Columns.Select(p => p.Header as string).ToArray();
                var tmpItems = this.resultGrid.SelectedCells.Select(p => p.Item).Distinct().Cast<DataRowView>().ToList();
                //var tmpItems = this.resultGrid.SelectedItems;
                if (tmpItems.Count < 1) return;

                var rowNames = Enumerable.Range(1, tmpItems.Count).Select(i => i.ToString()).ToArray();
                var verticalData = new object[rowNames.Length, columnNames.Length];

                for (int tmpRowIdx = 0; tmpRowIdx < tmpItems.Count; tmpRowIdx++)
                {
                    var rowAry = tmpItems[tmpRowIdx].Row.ItemArray;
                    for (int tmpClmIdx = 0; tmpClmIdx < rowAry.Length; tmpClmIdx++)
                    {
                        // 行と列が逆の配列にセット
                        verticalData[tmpRowIdx, tmpClmIdx] = rowAry[tmpClmIdx];
                    }
                }

                // RowとColumnを逆にし、データ部を縦横入れ替えたものをパラメータにする
                var dlg = new VerticalGridDialog(rowNames, columnNames, verticalData);
                dlg.Owner = this;
                dlg.ShowDialog();
            }
            catch (Exception ex)
            {
                SetDspTxt(ex.ToString());
            }
        }
        /// <summary>
        /// 表示中の検索結果のカラムをフィールド欄にセットします。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FieldSet_Click(object sender, RoutedEventArgs e)
        {
            FieldList = new ObservableCollection<FieldItem>(this.resultGrid.Columns.Select(p => new FieldItem() { FieldName = ((string)p.Header).Replace("__", "_") })); // 表示するときに"_"を増やしているので解除する
        }
    }

    /// <summary>
    /// AttributeCollection拡張クラス
    /// </summary>
    public static class AttributeCollectionExt
    {
        /// <summary>
        /// 画面表示用のObjectを取得します。
        /// 不正値の場合はnullを返します。
        /// </summary>
        /// <param name="attr"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static object GetDspValue(this AttributeCollection attr, string fieldName)
        {
            object obj;
            if (!attr.TryGetValue(fieldName, out obj) || obj == null) return null;
            
            if(obj is EntityReference)
            {
                var er = ((EntityReference)obj);
                return string.Format("[{0}]{1}{2}{3}", er.LogicalName, er.Name, Environment.NewLine, er.Id);
            }
            else if(obj is OptionSetValue)
            {
                return ((OptionSetValue)obj).Value;
            }
            return obj;
        }
    }
}
