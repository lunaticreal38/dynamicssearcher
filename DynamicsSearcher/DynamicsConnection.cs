﻿using Microsoft.Crm.Sdk.Messages;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Xrm.Tooling.Connector;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DynamicsSearcher
{
    /// <summary>
    /// Dynamics365と接続を行うクラス
    /// </summary>
    class DynamicsConnection : IDisposable
    {
        private readonly CrmServiceClient crmSvc;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="url">URL</param>
        /// <param name="userName">ユーザ名</param>
        /// <param name="password">パスワード</param>
        /// <exception cref="ArgumentNullException"/>
        public DynamicsConnection(string url, string userName, string password)
        {
            if (string.IsNullOrEmpty(url)) throw new ArgumentNullException("url");
            if (string.IsNullOrEmpty(userName)) throw new ArgumentNullException("userName");
            if (string.IsNullOrEmpty(password)) throw new ArgumentNullException("password");

            // ODatav4形式でCRMサーバと接続する
            crmSvc = new CrmServiceClient(string.Format("Url={0}; Username={1}; Password={2}; AuthType=Office365; RequireNewInstance=true;", url, userName, password));
            if (!crmSvc.IsReady) throw new ApplicationException("Dynamics接続に失敗");
        }

        /// <summary>
        /// 通信を行い、自身の接続情報を取得します。
        /// </summary>
        /// <returns>WhoAmIの応答</returns>
        public WhoAmIResponse WhoAmI()
        {
            var res = (WhoAmIResponse)crmSvc.Execute(new WhoAmIRequest());
            return res;
        }

        /// <summary>
        /// 通信を行い、応答結果を返します。
        /// </summary>
        /// <param name="request">通信要求</param>
        /// <exception cref="ArgumentNullException">パラメータがNullの場合にスロー</exception>
        /// <returns>応答結果</returns>
        public T Execute<T>(OrganizationRequest request) where T : OrganizationResponse
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            var paramStr = request is ExecuteTransactionRequest ? string.Join(",", ((ExecuteTransactionRequest)request).Requests.Select(p => p.RequestName))
                         : request is ExecuteMultipleRequest ? string.Join(",", ((ExecuteMultipleRequest)request).Requests.Select(p => p.RequestName))
                         : request.RequestName;
            return (T)crmSvc.Execute(request);
        }

        /// <summary>
        /// インスタンス破棄処理です。
        /// </summary>
        public void Dispose()
        {
            crmSvc.Dispose();
        }

        /// <summary>
        /// 検索クエリを実行し、Entityを取得します。
        /// ページング処理を行う場合はfetchQuery.PageInfoに設定を行って下さい。
        /// </summary>
        /// <param name="fetchQueryExpression">検索クエリ構文</param>
        /// <returns>Entityコレクション</returns>
        public EntityCollection GetEntityCollection(QueryExpression fetchQueryExpression)
        {
            EntityCollection entityCollection = new EntityCollection();
            var multiRequest = new RetrieveMultipleRequest { Query = fetchQueryExpression };
            RetrieveMultipleResponse multiResponse;
            do
            {
                multiResponse = (RetrieveMultipleResponse)crmSvc.Execute(multiRequest);

                // 取得したEntity
                entityCollection.Entities.AddRange(multiResponse.EntityCollection.Entities);
                if (multiResponse.EntityCollection.MoreRecords)
                {
                    if (fetchQueryExpression.PageInfo != null)
                    {
                        fetchQueryExpression.PageInfo.PageNumber++;
                    }
                }
            }
            while (multiResponse.EntityCollection.MoreRecords); // 未取得レコードが残っていれば繰り返す
            return entityCollection;
        }

        /// <summary>
        /// XML文字列から条件に従った検索用のクエリに変換します。
        /// </summary>
        /// <param name="fetchXML">取得XML文字列</param>
        /// <returns>検索クエリ構文</returns>
        public QueryExpression ConvertQueryExpression(string fetchXML)
        {
            var req = new FetchXmlToQueryExpressionRequest { FetchXml = fetchXML };
            var res = (FetchXmlToQueryExpressionResponse)crmSvc.Execute(req);
            return res.Query;
        }

        /// <summary>
        /// パラメータから検索用クエリに変換します。
        /// </summary>
        /// <param name="logcalName">検索するEntity名</param>
        /// <param name="fields">Entityのフィールド名</param>
        /// <returns>検索用クエリ</returns>
        public QueryExpression ConvertQueryExpression(string logcalName, IEnumerable<string> fields)
        {
            return ConvertQueryExpression(CreateSearchQuery(logcalName, fields));
        }
        /// <summary>
        /// パラメータからクエリ用のXMLを生成します。
        /// </summary>
        /// <param name="logcalName">検索するEntity名</param>
        /// <param name="fields">Entityのフィールド名</param>
        /// <returns>検索用クエリ</returns>
        public string CreateSearchQuery(string logcalName, IEnumerable<string> fields)
        {
            var root = CreateXElement("fetch", new[] { Tuple.Create("mapping", "logical") });
            var entity = CreateXElement("entity", new[] { Tuple.Create("name", logcalName) });
            root.Add(entity);

            if(fields.Any())
            {
                // 指定したフィールドだけ取得する
                foreach (var fld in fields)
                {
                    entity.Add(CreateXElement("attribute", new[] { Tuple.Create("name", fld) }));
                }
            }
            else
            {
                // 全フィールド取得
                entity.Add(CreateXElement(@"all-attributes"));
            }
            return root.ToString();
        }
        /// <summary>
        /// XElementを生成します。
        /// </summary>
        /// <param name="elmName">要素名</param>
        /// <param name="attributes">属性名,値 の列挙体</param>
        /// <returns>XElement</returns>
        private XElement CreateXElement(string elmName, IEnumerable<Tuple<string, string>> attributes = null)
        {
            var elm = new XElement(elmName);
            if (attributes != null)
            {
                foreach (var attr in attributes)
                {
                    elm.SetAttributeValue(attr.Item1, attr.Item2);
                }
            }
            return elm;
        }
    }
}
